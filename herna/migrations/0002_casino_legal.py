# -*- coding: utf-8 -*-
# Generated by Django 1.10a1 on 2016-06-24 09:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('herna', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='casino',
            name='legal',
            field=models.BooleanField(default=True),
        ),
    ]
