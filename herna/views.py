from django.shortcuts import render
from django.views.generic.detail import DetailView

from haystack.generic_views import SearchView

from models import Casino


class CasinoSearchView(SearchView):
    template_name = 'herna/search.html'

class CasinoDetailView(DetailView):
    model = Casino
    template_name = "herna/detail.html"
    fields = '__all__'