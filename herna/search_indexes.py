import datetime
from haystack import indexes
from herna.models import Casino


class NoteIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    obec = indexes.CharField(model_attr='obec', null=True)
    mestska_cast = indexes.CharField(model_attr='mestska_cast', null=True)
    cast_obce = indexes.CharField(model_attr='cast_obce', null=True)
    ulice = indexes.CharField(model_attr='ulice', null=True)
    cp = indexes.CharField(model_attr='cp', null=True)
    co = indexes.CharField(model_attr='co', null=True)
    psc = indexes.CharField(model_attr='psc', null=True)
    cislo_obce = indexes.CharField(model_attr='cislo_obce', null=True)
    kraj = indexes.CharField(model_attr='kraj', null=True)
    typ_cinnosti = indexes.CharField(model_attr='typ_cinnosti', null=True)
    provozovatel = indexes.CharField(model_attr='provozovatel', null=True)
    ic = indexes.CharField(model_attr='ic', null=True)
    povoleno_do = indexes.DateField(model_attr='povoleno_do', null=True)
    vyrobni_cislo = indexes.CharField(model_attr='vyrobni_cislo', null=True)

    def get_model(self):
        return Casino