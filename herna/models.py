# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Casino(models.Model):
    # Obec  
    # Městská část  
    # Část obce 
    # Ulice 
    # Č.p.  
    # Č.o.  
    # PSČ   
    # Číslo obce    
    # Kraj  
    # Typ činnosti* 
    # Provozovatel  
    # IČ    
    # Povoleno do   Výrobní číslo
    obec = models.CharField(max_length=64, null=True, blank=True)
    mestska_cast = models.CharField(max_length=64, null=True, blank=True)
    cast_obce = models.CharField(max_length=32, null=True, blank=True)
    ulice = models.CharField(max_length=128, null=True, blank=True)
    cp =  models.CharField(max_length=20, null=True, blank=True)
    co =  models.CharField(max_length=20, null=True, blank=True)
    psc =  models.CharField(max_length=20, null=True, blank=True)
    cislo_obce =  models.CharField(max_length=20, null=True, blank=True)
    kraj =  models.CharField(max_length=20, null=True, blank=True)
    typ_cinnosti = models.CharField(max_length=20, null=True, blank=True)
    provozovatel =  models.CharField(max_length=20, null=True, blank=True)
    ic =  models.CharField(max_length=20, null=True, blank=True)
    povoleno_do = models.DateField(null=True, blank=True)
    vyrobni_cislo = models.CharField(max_length=20, null=True, blank=True)
    legal = models.BooleanField(default=True)
    last_updated = models.DateField(auto_now=True)
    
    class Meta:
        verbose_name = "Casino"
        verbose_name_plural = "Casinos"

    def __unicode__(self):
        return '%s - %s - %s' % (self.obec, self.provozovatel, self.povoleno_do)