from django.conf.urls import url
from views import CasinoSearchView, CasinoDetailView

urlpatterns = [
    url(r'^search/$', CasinoSearchView.as_view(), name='search'),
    url(r'^detail/(?P<pk>[\d]+)/$', CasinoDetailView.as_view(), name='detail'),
]