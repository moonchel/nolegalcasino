from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

import os
from openpyxl import load_workbook

from herna.models import Casino

class Command(BaseCommand):
    help = 'update db with excel file, and reindex for solr'

    def add_arguments(self, parser):
        parser.add_argument(
            '-f',
            '--file',
            dest='filename',
            default=False,
            help='Provide file for database update',
        )

    def handle(self, *args, **options):
        filename = os.path.join(settings.BASE_DIR, 'herna', 'data', 'daty.xlsx')
        filename = options['filename'] if options['filename'] else filename
        wb = load_workbook(filename=filename, read_only=True)
        ws = wb.get_sheet_names()[0]

        rows = wb[ws].rows
        header = rows.next()
        fields = rows.next()
        bulk = []
        for row in rows:
            (obec, mestska_cast,
            cast_obce, ulice, cp,
            co, psc, cislo_obce,
            kraj, typ_cinnosti, 
            provozovatel, ic,
            povoleno_do, vyrobni_cislo) = (cell.value for cell in row)

            casino, created = Casino.objects.update_or_create(
                obec=obec, mestska_cast=mestska_cast,
                cast_obce=cast_obce, ulice=ulice, cp=cp,
                co=co, psc=psc, cislo_obce=cislo_obce,
                kraj=kraj, typ_cinnosti=typ_cinnosti,
                provozovatel=provozovatel,
                ic=ic, povoleno_do=povoleno_do,
                vyrobni_cislo=vyrobni_cislo
            )
            print casino, created
