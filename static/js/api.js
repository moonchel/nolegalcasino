$(document).ready(function(){
	var locations = [];

	var map = new google.maps.Map($('.map')[0], {
	    center: {lat: 0.0, lng: 0.0},
	    zoom: 8
	});

	$('#search_form').on('submit', function(event){
		event.preventDefault();
		if ($('#id_q').val().length < 2) {
			$('.results').html('<h3 class="ui header">Please, provide more characters...</h3>');
			return;
		}
		$.ajax({
			url : $(this).attr('action'),
			data : $(this).serialize(),
		}).done(function(data){
			$('.results').html(data);
			$('table').stupidtable();
			load_locations(data);
		});
	});

	function load_locations(data){
		var address;
		var bounds = new google.maps.LatLngBounds();

		$.each(locations, function(index, value){
			value.setMap(null);
			value.length = 0;
		});
		
		$('tr[hidden="hidden"] td').each(function(index, value){
			address = $.trim($(value).html());
			$.ajax({
				url : 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address,
				async : false
			}).done(function(data){
				if (data['status'] == "OK") {
					position = {
						lat : data['results'][0]['geometry']['location']['lat'],
						lng : data['results'][0]['geometry']['location']['lng'],
					}
					var marker = new google.maps.Marker({
						position: position,
						map: map,
						title: '' + index
					});
					locations.push(marker);
				}
			});
		});
		
		$.each(locations, function(index, value){
			bounds.extend(value.getPosition());
		});

		map.fitBounds(bounds);
	}


	$('.results').on('click', '.pagination a', function (event) {
		event.preventDefault();
		$.ajax({
			url : $(this).attr('href')
		}).done(function (data) {
			$('.results').html(data);
			$('table').stupidtable();
			load_locations(data);
		});
	});

	$('.results').on('click', '.link', function (event) {
		event.preventDefault();
		$('.clicked').removeClass('clicked');
		$(this).addClass('clicked');
		$.ajax({
			url : $(this).attr('href')
		}).done(function(data){
			$('.detail').html(data);
		});
	});

});